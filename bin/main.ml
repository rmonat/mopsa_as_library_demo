let () =
  let mopsa_dir = "/home/raphael/work/mopsa" in
  let args = [|
    "lib call";
    "-share-dir=" ^ mopsa_dir ^ "/share/mopsa";
    "-config=universal/default.json";
    "-debug=_";
    "analyzer/tests/universal/int_tests.u"
  |] in
  let open Mopsa_analyzer.Framework.Runner in
  exit @@ parse_options args analyze_files ()
